package application;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Generalitati {
	private final IntegerProperty IdGeneralitati;
	private final IntegerProperty IdPlanta;
	private final StringProperty NumePlanta;
	private final StringProperty NumeStiintificPlanta;
	private final StringProperty DataDescoperirePlanta;
	private final IntegerProperty IdCaracteristici;
	private final StringProperty TipReproducere;
	private final StringProperty TipPlanta;
	private final StringProperty Culoare;
	private final StringProperty Sezon;
	private final StringProperty Specie;
	private final StringProperty Regiune;
	private final StringProperty Mediu;
	
	public Generalitati(
		Integer IdGeneralitati, Integer IdPlanta, String NumePlanta, String NumeStiintificPlanta, 
		String DataDescoperirePlanta, Integer IdCaracteristici, String TipReproducere, String TipPlanta, 
		String Culoare, String Sezon, String Specie, String Regiune, String Mediu) {
		
		this.IdGeneralitati = new SimpleIntegerProperty(IdGeneralitati);
		this.IdPlanta = new SimpleIntegerProperty(IdPlanta);
		this.NumePlanta = new SimpleStringProperty(NumePlanta);
		this.NumeStiintificPlanta = new SimpleStringProperty(NumeStiintificPlanta);
		this.DataDescoperirePlanta = new SimpleStringProperty(DataDescoperirePlanta);
		this.IdCaracteristici = new SimpleIntegerProperty(IdCaracteristici);
		this.TipReproducere = new SimpleStringProperty(TipReproducere);
		this.TipPlanta = new SimpleStringProperty(TipPlanta);
		this.Culoare = new SimpleStringProperty(Culoare);
		this.Sezon = new SimpleStringProperty(Sezon);
		this.Specie = new SimpleStringProperty(Specie);
		this.Regiune = new SimpleStringProperty(Regiune);
		this.Mediu = new SimpleStringProperty(Mediu);
	}
	
	public Integer getIdGeneralitati() {
		return IdGeneralitati.get();
	}
	
	public Integer getIdPlanta() {
		return IdPlanta.get();
	}
	
	public String getNumePlanta() {
		return NumePlanta.get();
	}
	
	public String getNumeStiintificPlanta() {
		return NumeStiintificPlanta.get();
	}
	
	public String getDataDescoperirePlanta() {
		return DataDescoperirePlanta.get();
	}
	
	public Integer getIdCaracteristici() {
		return IdCaracteristici.get();
	}
	
	public String getTipReproducere() {
		return TipReproducere.get();
	}
	
	public String getTipPlanta() {
		return TipPlanta.get();
	}
	
	public String getCuloare() {
		return Culoare.get();
	}
	
	public String getSezon() {
		return Sezon.get();
	}
	
	public String getSpecie() {
		return Specie.get();
	}
	
	public String getRegiune() {
		return Regiune.get();
	}
	
	public String getMediu() {
		return Mediu.get();
	}
	
	public void setIdGeneralitati(Integer valoare) {
		IdGeneralitati.set(valoare);
	}
	
	public void setIdPlanta(Integer valoare) {
		IdPlanta.set(valoare);
	}
	
	public void setNumePlanta(String valoare) {
		NumePlanta.set(valoare);
	}
	
	public void setNumeStiintificPlanta(String valoare) {
		NumeStiintificPlanta.set(valoare);
	}
	
	public void setDataDescoperirePlanta(String valoare) {
		DataDescoperirePlanta.set(valoare);
	}
	
	public void setIdCaracteristici(Integer valoare) {
		IdCaracteristici.set(valoare);
	}
	
	public void setTipReproducere(String valoare) {
		TipReproducere.set(valoare);
	}
	
	public void setTipPlanta(String valoare) {
		TipPlanta.set(valoare);
	}
	
	public void setCuloare(String valoare) {
		Culoare.set(valoare);
	}
	
	public void setSezon(String valoare) {
		Sezon.set(valoare);
	}
	
	public void setSpecie(String valoare) {
		Specie.set(valoare);
	}
	
	public void setRegiune(String valoare) {
		Regiune.set(valoare);
	}
	
	public void setMediu(String valoare) {
		Mediu.set(valoare);
	}
	
	public IntegerProperty IdGeneralitatiProperty() {
		return IdGeneralitati;
	}
	
	public IntegerProperty IdPlantaProperty() {
		return IdPlanta;
	}
	
	public StringProperty NumePlantaProperty() {
		return NumePlanta;
	}
	
	public StringProperty NumeStiintificPlantaProperty() {
		return NumeStiintificPlanta;
	}
	
	public StringProperty DataDescoperirePlantaProperty() {
		return DataDescoperirePlanta;
	}
	
	public IntegerProperty IdCaracteristiciProperty() {
		return IdCaracteristici;
	}
	
	public StringProperty TipReproducereProperty() {
		return TipReproducere;
	}
	
	public StringProperty TipPlantaProperty() {
		return TipPlanta;
	}
	
	public StringProperty CuloareProperty() {
		return Culoare;
	}
	
	public StringProperty SezonProperty() {
		return Sezon;
	}
	
	public StringProperty SpecieProperty() {
		return Specie;
	}
	
	public StringProperty RegiuneProperty() {
		return Regiune;
	}
	
	public StringProperty MediuProperty() {
		return Mediu;
	}
}
