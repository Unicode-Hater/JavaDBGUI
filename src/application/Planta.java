package application;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Planta {
	private final IntegerProperty IdPlanta;
	private final StringProperty Nume;
	private final StringProperty NumeStiintific;
	private final StringProperty DataDescoperire;
	
	public Planta(Integer IdPlanta, String Nume,String NumeStiintific, String DataDescoperire) {
		this.IdPlanta = new SimpleIntegerProperty(IdPlanta);
		this.Nume = new SimpleStringProperty(Nume);
		this.NumeStiintific = new SimpleStringProperty(NumeStiintific);
		this.DataDescoperire = new SimpleStringProperty(DataDescoperire);
	}
	
	public String toString() {
		return "Id: '" + getIdPlanta().toString() + "', Nume: '" + getNume() + "'";
	}
	
	public Integer getIdPlanta() {
		return IdPlanta.get();
	}
	
	public String getNume() {
		return Nume.get();
	}
	
	public String getNumeStiintific() {
		return NumeStiintific.get();
	}
	
	public String getDataDescoperire() {
		return DataDescoperire.get();
	}
	
	public void setIdPlanta(Integer valoare) {
		IdPlanta.set(valoare);
	}
	
	public void setNume(String valoare) {
		Nume.set(valoare);
	}
	
	public void setNumeStiintific(String valoare) {
		NumeStiintific.set(valoare);
	}
	
	public void setDataDescoperire(String valoare) {
		DataDescoperire.set(valoare);
	}
	
	public IntegerProperty IdPlantaProperty() {
		return IdPlanta;
	}
	
	public StringProperty NumeProperty() {
		return Nume;
	}
	
	public StringProperty NumeStiintificProperty() {
		return NumeStiintific;
	}
	
	public StringProperty DataDescoperireProperty() {
		return DataDescoperire;
	}
}
