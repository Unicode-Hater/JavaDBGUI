package application;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;

import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.fxml.FXMLLoader;
import java.io.IOException;

import java.sql.Date;
//import javafx.scene.control.Label;

public class ProjectController implements Initializable {
	//private Label label;

	@FXML
	private Tab tab_Plante;
	
	@FXML
	private TableView<Planta> tabela_Plante;

	@FXML
	private Button buton_AdaugaPlanta;
	
	@FXML
	private Button buton_ModificaPlanta;
	
	@FXML
	private Button buton_StergePlante;

	@FXML
	private TableColumn<?, ?> atribut_IdPlanta;

	@FXML
	private TableColumn<?, ?> atribut_Nume;

	@FXML
	private TableColumn<?, ?> atribut_NumeStiintific;

	@FXML
	private TableColumn<?, ?> atribut_DataDescoperire;

	@FXML
	private Tab tab_Caracteristici;
	
	@FXML
	private TableView<Caracteristici> tabela_Caracteristici;

	@FXML
	private Button buton_AdaugaCaracteristica;
	
	@FXML
	private Button buton_ModificaCaracteristica;
	
	@FXML
	private Button buton_StergeCaracteristici;
	
	@FXML
	private TableColumn<?, ?> atribut_IdCaracteristici;

	@FXML
	private TableColumn<?, ?> atribut_Reproducere;

	@FXML
	private TableColumn<?, ?> atribut_Tip;

	@FXML
	private TableColumn<?, ?> atribut_Culoare;

	@FXML
	private TableColumn<?, ?> atribut_Sezon;

	@FXML
	private Tab tab_Generalitati;
	
	@FXML
	private TableView<Generalitati> tabela_Generalitati;
	
	@FXML
	private Button buton_AdaugaGeneralitate;
	
	@FXML
	private Button buton_ModificaGeneralitate;
	
	@FXML
	private Button buton_StergeGeneralitati;

	@FXML
	private TableColumn<?, ?> atribut_IdGeneralitati;

	@FXML
	private TableColumn<?, ?> atribut_IdPlantaG;

	@FXML
	private TableColumn<?, ?> atribut_NumePlanta;

	@FXML
	private TableColumn<?, ?> atribut_NumeStiintificPlanta;

	@FXML
	private TableColumn<?, ?> atribut_DataDescoperirePlanta;

	@FXML
	private TableColumn<?, ?> atribut_IdCaracteristiciG;

	@FXML
	private TableColumn<?, ?> atribut_TipReproducere;

	@FXML
	private TableColumn<?, ?> atribut_TipPlanta;

	@FXML
	private TableColumn<?, ?> atribut_CuloareG;

	@FXML
	private TableColumn<?, ?> atribut_SezonG;

	@FXML
	private TableColumn<?, ?> atribut_Specie;

	@FXML
	private TableColumn<?, ?> atribut_Regiune;
	
	@FXML
	private TableColumn<?, ?> atribut_Mediu;

	private static ObservableList<Planta> datePlante;
	private static ObservableList<Caracteristici> dateCaracteristici;
	private static ObservableList<Generalitati> dateGeneralitati;
	
	private static Planta selected_Planta;
	private static Caracteristici selected_Caracteristica;
	private static Generalitati selected_Generalitate;
	private DBOperations jb;
	
	@FXML
	private TextField atribut_NouNume;
	
	@FXML
	private TextField atribut_NouNumeStiintific;
	
	@FXML
	private TextField atribut_NouDataDescoperire;
	
	@FXML
	private TextField atribut_NouReproducere;

	@FXML
	private TextField atribut_NouTip;
	
	@FXML
	private TextField atribut_NouCuloare;
	
	@FXML
	private TextField atribut_NouSezon;
	
	@FXML
	private ComboBox<Planta> atribut_NouPlanta;
	
	@FXML
	private ComboBox<Caracteristici> atribut_NouCaracteristica;
	
	@FXML
	private TextField atribut_NouSpecie;
	
	@FXML
	private TextField atribut_NouRegiune;
	
	@FXML
	private TextField atribut_NouMediu;
	
	@FXML
	private Button atribut_Adauga;
	
	@FXML
	private Button atribut_Modifica;

	private void createJB() {
		if (jb == null) {
			jb = new DBOperations();
		}
	}
	
	private void startApp(String UIPath) throws IOException {
		Stage stage = new Stage();
		HBox root = (HBox)FXMLLoader.load(getClass().getResource(UIPath));
		Scene scene = new Scene(root);
		stage.setResizable(false);
		stage.setScene(scene);
		stage.show();
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		createJB();
		
		try {			
			if (atribut_NouPlanta != null) {
				datePlante = FXCollections.observableArrayList();
				jb.connect();
				ResultSet rs = jb.vedeTabela("plante");
				
				while (rs.next()) {
					datePlante.add(
						new Planta(
							rs.getInt("idplanta"), rs.getString("nume"), 
							rs.getString("nume_stiintific"), rs.getDate("data_descoperire").toString()
						)
					);
				}
				
				jb.disconnect();
				atribut_NouPlanta.setItems(datePlante);
			}
			
			if (atribut_NouCaracteristica != null) {
				jb.connect();
				dateCaracteristici = FXCollections.observableArrayList();
				ResultSet rs = jb.vedeTabela("caracteristici");
				
				while (rs.next()) {
					dateCaracteristici.add(
						new Caracteristici(
							rs.getInt("idcaracteristici"), rs.getString("reproducere"), 
							rs.getString("tip"), rs.getString("culoare"), rs.getString("sezon")
						)
					);
				}
				
				jb.disconnect();
				atribut_NouCaracteristica.setItems(dateCaracteristici);
			}
		} catch (Exception ex) {
			System.err.println(ex.toString());
		}
	}

	@FXML
	private void incarcaPlante(Event event) throws SQLException, Exception {
		createJB();
		
		jb.connect();
		datePlante = FXCollections.observableArrayList();
		ResultSet rs = jb.vedeTabela("plante");
		
		while (rs.next()) {
			datePlante.add(
				new Planta(
					rs.getInt("idplanta"), rs.getString("nume"), 
					rs.getString("nume_stiintific"), rs.getDate("data_descoperire").toString()
				)
			);
		}
		
		atribut_IdPlanta.setCellValueFactory(new PropertyValueFactory<>("IdPlanta"));
		atribut_Nume.setCellValueFactory(new PropertyValueFactory<>("Nume"));
		atribut_NumeStiintific.setCellValueFactory(new PropertyValueFactory<>("NumeStiintific"));
		atribut_DataDescoperire.setCellValueFactory(new PropertyValueFactory<>("DataDescoperire"));
		
		tabela_Plante.setItems(null);
		tabela_Plante.setItems(datePlante);
		
		jb.disconnect();
	}

	@FXML
	private void incarcaCaracteristici(Event event) throws SQLException, Exception {
		createJB();
		
		jb.connect();
		dateCaracteristici = FXCollections.observableArrayList();
		ResultSet rs = jb.vedeTabela("caracteristici");
		
		while (rs.next()) {
			dateCaracteristici.add(
				new Caracteristici(
					rs.getInt("idcaracteristici"), rs.getString("reproducere"), 
					rs.getString("tip"), rs.getString("culoare"), rs.getString("sezon")
				)
			);
		}
		
		atribut_IdCaracteristici.setCellValueFactory(new PropertyValueFactory<>("IdCaracteristica"));
		atribut_Reproducere.setCellValueFactory(new PropertyValueFactory<>("Reproducere"));
		atribut_Tip.setCellValueFactory(new PropertyValueFactory<>("Tip"));
		atribut_Culoare.setCellValueFactory(new PropertyValueFactory<>("Culoare"));
		atribut_Sezon.setCellValueFactory(new PropertyValueFactory<>("Sezon"));
		
		tabela_Caracteristici.setItems(null);
		tabela_Caracteristici.setItems(dateCaracteristici);
		
		jb.disconnect();
	}
	
	@FXML
	private void incarcaGeneralitati(Event event) throws SQLException, Exception {
		createJB();
		
		jb.connect();
		dateGeneralitati = FXCollections.observableArrayList();
		ResultSet rs = jb.vedeGeneralitati();
		
		while (rs.next()) {
			dateGeneralitati.add(
				new Generalitati(
					rs.getInt("idgeneralitati"), rs.getInt("idplanta"), rs.getString("nume"), 
					rs.getString("nume_stiintific"), rs.getDate("data_descoperire").toString(),
					rs.getInt("idcaracteristici"), rs.getString("reproducere"), rs.getString("tip"),
					rs.getString("culoare"), rs.getString("sezon"), rs.getString("specie"),
					rs.getString("regiune"), rs.getString("mediu")
				)
			);
		}
		
		atribut_IdGeneralitati.setCellValueFactory(new PropertyValueFactory<>("IdGeneralitati"));
		atribut_IdPlantaG.setCellValueFactory(new PropertyValueFactory<>("IdPlanta"));
		atribut_NumePlanta.setCellValueFactory(new PropertyValueFactory<>("NumePlanta"));
		atribut_NumeStiintificPlanta.setCellValueFactory(new PropertyValueFactory<>("NumeStiintificPlanta"));
		atribut_DataDescoperirePlanta.setCellValueFactory(new PropertyValueFactory<>("DataDescoperirePlanta"));
		atribut_IdCaracteristiciG.setCellValueFactory(new PropertyValueFactory<>("IdCaracteristici"));
		atribut_TipReproducere.setCellValueFactory(new PropertyValueFactory<>("TipReproducere"));
		atribut_TipPlanta.setCellValueFactory(new PropertyValueFactory<>("TipPlanta"));
		atribut_CuloareG.setCellValueFactory(new PropertyValueFactory<>("Culoare"));
		atribut_SezonG.setCellValueFactory(new PropertyValueFactory<>("Sezon"));
		atribut_Specie.setCellValueFactory(new PropertyValueFactory<>("Specie"));
		atribut_Regiune.setCellValueFactory(new PropertyValueFactory<>("Regiune"));
		atribut_Mediu.setCellValueFactory(new PropertyValueFactory<>("Mediu"));
		
		tabela_Generalitati.setItems(null);
		tabela_Generalitati.setItems(dateGeneralitati);
		
		jb.disconnect();
	}
	
	@FXML
	private void stergePlante(Event event) throws SQLException, Exception {
		jb.connect();
		
		Planta planta = tabela_Plante.getSelectionModel().getSelectedItem();
		datePlante.remove(planta);
		String[] id = {planta.getIdPlanta().toString()};
		
		jb.stergeDateTabela(id, "plante", "idplanta");
		jb.disconnect();
	}
	
	@FXML
	private void stergeCaracteristici(Event event) throws SQLException, Exception {
		jb.connect();
		
		Caracteristici carac = tabela_Caracteristici.getSelectionModel().getSelectedItem();
		dateCaracteristici.remove(carac);
		String[] id = {carac.getIdCaracteristica().toString()};
		
		jb.stergeDateTabela(id, "caracteristici", "idcaracteristici");
		jb.disconnect();
	}
	
	@FXML
	private void stergeGeneralitati(Event event) throws SQLException, Exception {
		jb.connect();
		
		Generalitati gener = tabela_Generalitati.getSelectionModel().getSelectedItem();
		dateGeneralitati.remove(gener);
		String[] id = {gener.getIdGeneralitati().toString()};
		
		jb.stergeDateTabela(id, "generalitati", "idgeneralitati");
		jb.disconnect();
	}
	
	@FXML
	private void startModificaPlanta(Event event) throws IOException {
		selected_Planta = tabela_Plante.getSelectionModel().getSelectedItem();
		startApp("ModificaPlantaUI.fxml");
	}
	
	@FXML
	private void startModificaCaracteristica(Event event) throws IOException {
		selected_Caracteristica = tabela_Caracteristici.getSelectionModel().getSelectedItem();
		startApp("ModificaCaracteristicaUI.fxml");
	}
	
	@FXML
	private void startModificaGeneralitate(Event event) throws IOException {
		selected_Generalitate = tabela_Generalitati.getSelectionModel().getSelectedItem();
		startApp("ModificaGeneralitateUI.fxml");
	}
	
	@FXML
	private void modificaPlanta(Event event) throws SQLException, Exception {
		if (selected_Planta == null) {
			throw new Exception("Planta Inexistenta!!");
		}
		
		jb.connect();
		
		String nume = atribut_NouNume.getText().toString();
		String nume_stiintific = atribut_NouNumeStiintific.getText().toString();
		String data_descoperire = atribut_NouDataDescoperire.getText().toString();
		
		String[] valori = {nume, nume_stiintific, data_descoperire};
		String[] campuri = {"nume", "nume_stiintific", "data_descoperire"};
		
		jb.modificaTabela("plante", "idplanta", selected_Planta.getIdPlanta(), campuri, valori);
		datePlante.set(
			datePlante.indexOf(selected_Planta), new Planta(selected_Planta.getIdPlanta(), nume, nume_stiintific, data_descoperire)
		);
		
		selected_Planta = null;
		jb.disconnect();
		
		((Stage) (atribut_Modifica.getScene().getWindow())).close();
	}
	
	@FXML
	private void modificaCaracteristica(Event event) throws SQLException, Exception {
		if (selected_Caracteristica == null) {
			throw new Exception("Caracteristica Inexistenta!!");
		}
		
		jb.connect();
		
		String reproducere = atribut_NouReproducere.getText().toString();
		String tip = atribut_NouTip.getText().toString();
		String culoare = atribut_NouCuloare.getText().toString();
		String sezon = atribut_NouSezon.getText().toString();
		
		String[] valori = {reproducere, tip, culoare, sezon};
		String[] campuri = {"reproducere", "tip", "culoare", "sezon"};
		
		jb.modificaTabela("caracteristici", "idcaracteristici", selected_Caracteristica.getIdCaracteristica(), campuri, valori);
		dateCaracteristici.set(
			dateCaracteristici.indexOf(selected_Caracteristica), 
			new Caracteristici(selected_Caracteristica.getIdCaracteristica(), reproducere, tip, culoare, sezon)
		);
		
		selected_Caracteristica = null;
		jb.disconnect();
		
		((Stage) (atribut_Modifica.getScene().getWindow())).close();
	}
	
	@FXML
	private void modificaGeneralitate(Event event) throws SQLException, Exception {
		if (selected_Generalitate == null) {
			throw new Exception("Generalitate Inexistenta!!");
		}
		
		jb.connect();
		
		String specie = atribut_NouSpecie.getText().toString();
		String regiune = atribut_NouRegiune.getText().toString();
		String mediu = atribut_NouMediu.getText().toString();
		Integer id1 = atribut_NouPlanta.getSelectionModel().getSelectedItem().getIdPlanta();
		Integer id2 = atribut_NouCaracteristica.getSelectionModel().getSelectedItem().getIdCaracteristica();
		
		String[] valori = {id1.toString(), id2.toString(), specie, regiune, mediu};
        String[] campuri = {"idplanta", "idcaracteristici", "specie", "regiune", "mediu"};
        jb.modificaTabela("generalitati", "idgeneralitati", selected_Generalitate.getIdGeneralitati(), campuri, valori);
		ResultSet rs = jb.vedeGeneralitati(); 
		
		if (!rs.last()) {
			throw new Exception("ResultSet Gol!!");
		}
		
		dateGeneralitati.set(
			dateGeneralitati.indexOf(selected_Generalitate), new Generalitati(
				rs.getInt("idgeneralitati"), rs.getInt("idplanta"), rs.getString("nume"), 
				rs.getString("nume_stiintific"), rs.getDate("data_descoperire").toString(),
				rs.getInt("idcaracteristici"), rs.getString("reproducere"), rs.getString("tip"),
				rs.getString("culoare"), rs.getString("sezon"), rs.getString("specie"),
				rs.getString("regiune"), rs.getString("mediu")
			)
		);

		selected_Generalitate = null;
		jb.disconnect();
		
		((Stage) (atribut_Modifica.getScene().getWindow())).close();
	}
	
	@FXML
	private void startAdaugaPlanta(Event event) throws IOException {
		startApp("NouPlantaUI.fxml");
	}
	
	@FXML
	private void startAdaugaCaracteristica(Event event) throws IOException {
		startApp("NouCaracteristicaUI.fxml");
	}
	
	@FXML
	private void startAdaugaGeneralitate(Event event) throws IOException {
		startApp("NouGeneralitateUI.fxml");
	}
	
	@FXML
	private void adaugaPlanta(Event event) throws SQLException, Exception {
		String nume = atribut_NouNume.getText().toString();
		String nume_stiintific = atribut_NouNumeStiintific.getText().toString();
		Date data_descoperire = Date.valueOf(atribut_NouDataDescoperire.getText().toString());
		
		jb.connect();
		jb.adaugaPlanta(nume, nume_stiintific, data_descoperire);
		ResultSet rs = jb.vedeTabela("plante"); 
		
		if (!rs.last()) {
			throw new Exception("ResultSet Gol!!");
		}
		
		datePlante.add(
			new Planta(rs.getInt("idplanta"), nume, nume_stiintific, data_descoperire.toString())
		);

		jb.disconnect();
		
		((Stage) (atribut_Adauga.getScene().getWindow())).close();
	}
	
	@FXML
	private void adaugaCaracteristica(Event event) throws SQLException, Exception {
		String reproducere = atribut_NouReproducere.getText().toString();
		String tip = atribut_NouTip.getText().toString();
		String culoare = atribut_NouCuloare.getText().toString();
		String sezon = atribut_NouSezon.getText().toString();
		
		jb.connect();
		jb.adaugaCaracteristici(reproducere, tip, culoare, sezon);
		ResultSet rs = jb.vedeTabela("caracteristici"); 
		
		if (!rs.last()) {
			throw new Exception("ResultSet Gol!!");
		}
		
		dateCaracteristici.add(
			new Caracteristici(rs.getInt("idcaracteristici"), reproducere, tip, culoare, sezon)
		);

		jb.disconnect();
		
		((Stage) (atribut_Adauga.getScene().getWindow())).close();
	}
	
	@FXML
	private void adaugaGeneralitate(Event event) throws SQLException, Exception {
		String specie = atribut_NouSpecie.getText().toString();
		String regiune = atribut_NouRegiune.getText().toString();
		String mediu = atribut_NouMediu.getText().toString();
		Integer id1 = atribut_NouPlanta.getSelectionModel().getSelectedItem().getIdPlanta();
		Integer id2 = atribut_NouCaracteristica.getSelectionModel().getSelectedItem().getIdCaracteristica();
		
		jb.connect();
		jb.adaugaGeneralitate(id1, id2, specie, regiune, mediu);
		ResultSet rs = jb.vedeGeneralitati(); 
		
		if (!rs.last()) {
			throw new Exception("ResultSet Gol!!");
		}
		
		dateGeneralitati.add(
			new Generalitati(
				rs.getInt("idgeneralitati"), rs.getInt("idplanta"), rs.getString("nume"), 
				rs.getString("nume_stiintific"), rs.getDate("data_descoperire").toString(),
				rs.getInt("idcaracteristici"), rs.getString("reproducere"), rs.getString("tip"),
				rs.getString("culoare"), rs.getString("sezon"), rs.getString("specie"),
				rs.getString("regiune"), rs.getString("mediu")
			)
		);

		jb.disconnect();
		
		((Stage) (atribut_Adauga.getScene().getWindow())).close();
	}
}