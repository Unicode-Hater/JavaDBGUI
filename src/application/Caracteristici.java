package application;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Caracteristici {
	private final IntegerProperty IdCaracteristica;
	private final StringProperty Reproducere;
	private final StringProperty Tip;
	private final StringProperty Culoare;
	private final StringProperty Sezon;
	
	public Caracteristici(Integer IdCaracteristica, String Reproducere, String Tip, String Culoare, String Sezon) {
		this.IdCaracteristica = new SimpleIntegerProperty(IdCaracteristica);
		this.Reproducere = new SimpleStringProperty(Reproducere);
		this.Tip = new SimpleStringProperty(Tip);
		this.Culoare = new SimpleStringProperty(Culoare);
		this.Sezon = new SimpleStringProperty(Sezon);
	}
	
	public String toString() {
		return "Id: '" + getIdCaracteristica().toString() + "', Tip: '" + getTip() + "'";
	}
	
	public Integer getIdCaracteristica() {
		return IdCaracteristica.get();
	}
	
	public String getReproducere() {
		return Reproducere.get();
	}
	
	public String getTip() {
		return Tip.get();
	}
	
	public String getCuloare() {
		return Culoare.get();
	}
	
	public String getSezon() {
		return Sezon.get();
	}
	
	public void setIdCaracteristica(Integer valoare) {
		IdCaracteristica.set(valoare);
	}
	
	public void setReproducere(String valoare) {
		Reproducere.set(valoare);
	}
	
	public void setTip(String valoare) {
		Tip.set(valoare);
	}
	
	public void setCuloare(String valoare) {
		Culoare.set(valoare);
	}
	
	public void setSezon(String valoare) {
		Sezon.set(valoare);
	}
	
	public IntegerProperty IdCaracteristicaProperty() {
		return IdCaracteristica;
	}
	
	public StringProperty ReproducereProperty() {
		return Reproducere;
	}
	
	public StringProperty TipProperty() {
		return Tip;
	}
	
	public StringProperty CuloareProperty() {
		return Culoare;
	}
	
	public StringProperty SezonProperty() {
		return Sezon;
	}
}
